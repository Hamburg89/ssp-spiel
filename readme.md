# Stein Schere Papier (Produktname: SSP) 

Dieses Projekt zeigt am Beispiel eines Stein Schere Papier-Spiels exemplarisch die Zusammenarbeit zwischen Angular und Spring.
 

## Frontend-Entwicklung

### Frontend starten
1. Voraussetzungen: 
  * npm ist installiert.
2. in das Verzeichnis src/main/js wechseln.
3. 'npm install' ausführen
4. 'npm start' aufrufen

## Backend-Entwicklung

* Das Projekt clonen
* Mit Maven alle Abhängigkeiten auflösen
* Den Spring Boot-Service über die IDE starten


## Release bauen
1. In das Verzeichnis src/main/js wechseln.
2. `npm run-script build` ausführen
3. mvn clean install ausführen
* In der pom.xml ist ein Task hinterlegt, welcher das Build des Frontends dem Maven-Build hinzufügt.


## Service-Endpunkte

* http://localhost:8080/actuator
* http://localhost:8080/game
* http://localhost:8080/games

## Postman

Im Ordner Postman ist eine Collection für das Programm Postman (https://www.getpostman.com/downloads/)
hinterlegt, mit welchem man exemplarisch die Service-Endpunkte aufrufen kann.

## Sonstiges

Aktuell sind für einen produktiven Einsatz noch mehrere Sachen zu konfigurieren. Die Ports müssen noch richtig 
konfiguriert werden und entsprechend einem Reverse Proxy angepasst werden (Empfehlung HAProxy).

