package hamburg.bigdata.ssp.game.model;

public class PieceFactory {
    public static Piece create(PieceType type) {
        switch (type){
            case Stein: return new Piece(PieceType.Stein, PieceType.Schere, PieceType.Papier);
            case Schere: return new Piece(PieceType.Schere, PieceType.Papier, PieceType.Stein);
            case Papier: return new Piece(PieceType.Papier, PieceType.Stein, PieceType.Schere);
            default: return null;
        }
    }
}
