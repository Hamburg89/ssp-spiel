import { Component, OnInit } from '@angular/core';
import {Game} from "../game";
import {GameService} from "../game.service";
import {Data} from "@angular/router";

@Component({
  selector: 'app-gameplay',
  templateUrl: './gameplay.component.html',
  styleUrls: ['./gameplay.component.css']
})
export class GameplayComponent implements OnInit {

  game: Game = new Game();
  submitted = false;
  humanChoice:String = '';
  computerChoice:String = '';
  result:String = '';

  constructor(private gameService: GameService) { }

  ngOnInit() {
  }

  newGame(choice: string): void {
    this.game = new Game();
    this.game.humanChoice = choice;
  }

  selectStein() {
    this.createGame('Stein')
  }

  selectSchere() {
    this.createGame('Schere')
  }

  selectPapier() {
    this.createGame('Papier')
  }

  createGame(choice: string){
    this.newGame(choice);
    this.gameService.createGame(this.game)
      .subscribe(data => this.evaluate(data), error => console.log(error));
  }

  evaluate(data: Data){
    console.log(data);
    this.game = data as Game;
    this.result = this.game.result;
    this.computerChoice = this.game.computerChoice;
    this.humanChoice = this.game.humanChoice
    this.submitted = true;
  }

}
