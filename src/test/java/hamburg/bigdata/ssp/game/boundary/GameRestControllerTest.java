package hamburg.bigdata.ssp.game.boundary;

import hamburg.bigdata.ssp.game.control.GameRepository;
import hamburg.bigdata.ssp.game.control.GameService;
import hamburg.bigdata.ssp.game.model.GamePo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static hamburg.bigdata.ssp.game.boundary.GameInJoExampleBuilder.createDefaultGameInJo;
import static hamburg.bigdata.ssp.game.boundary.GameOutJoExampleBuilder.createDefaultGameOutJo;
import static hamburg.bigdata.ssp.game.model.GamePoExampleBuilder.createDefaultGamePo;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class GameRestControllerTest {
    private final GameOutJoMapper gameOutJoMapper = new GameOutJoMapper();

    private final GameInJoMapper gameInJoMapper = new GameInJoMapper();

    @Autowired
    private GameService gameSrv;

    @Autowired
    GameRepository repository;

    @Autowired
    private TestRestTemplate restTemplate;

    private GamePo createGame() {
        GamePo game = createDefaultGamePo();
        game = gameSrv.save(game);
        return game;
    }

    @Test
    public void shouldPlayGame() {
        GameOutJo gameOutJo = createDefaultGameOutJo();
        //given
        HttpEntity<GameInJo> entity = new HttpEntity<>(createDefaultGameInJo());

        //when
        ResponseEntity<GameOutJo> response = restTemplate.postForEntity("/api/game", entity, GameOutJo.class);

        // then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody().getId()).isNull();
        assertThat(response.getBody().getHumanChoice()).isEqualTo(gameOutJo.getHumanChoice());
        assertThat(response.getBody().getComputerChoice()).isNotNull();
        assertThat(response.getBody().getResult()).isNotNull();
    }

    @Test
    public void shouldGetGames() {
        GamePo gamePo = createGame();

        //given
        HttpEntity<String> entity = new HttpEntity<>("");

        //when
        //ToDo Als Respone wird List<GameOutJo> geliefert. In dieser Testimplementierung ist noch GameOutJo vorhanden. Überarbeitung notwendig ggf. durch neues Objekt GamesOutJo.
        ResponseEntity<GameOutJo> response = restTemplate.exchange("/api/games", HttpMethod.GET, entity, GameOutJo.class);

        // then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody().getId()).isEqualTo(1);
        assertThat(response.getBody().getHumanChoice()).isEqualTo(gamePo.getHumanChoice());
        assertThat(response.getBody().getComputerChoice()).isEqualTo(gamePo.getComputerChoice());
        assertThat(response.getBody().getResult()).isEqualTo(gamePo.getResult());
    }
}
